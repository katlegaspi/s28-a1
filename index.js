const url = 'https://jsonplaceholder.typicode.com/todos'
const urlOne = 'https://jsonplaceholder.typicode.com/todos/1'
// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API (https://jsonplaceholder.typicode.com/todos/).
// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
async function fetchData(){
	let result = await fetch(url);
	let json = await result.json();
	let map = await json.map(function(json){
		return json.title;
	});
	console.log(map);
};
fetchData();

// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch(urlOne).then((response) => response.json()).then((json) => console.log(json));
// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch(urlOne).then((res) => res.json()).then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}.`));

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch(url,{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "New todo list item",
    completed: true,
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
/* Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID */
fetch(urlOne,{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated todo list item',
		description: 'Submit activity on Boodle',
		status: 'Doing',
		dateCompleted: 'Pending',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch(urlOne,{
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "FINAL!! I swear I'm going to finish this",
		status: 'Complete',
		dateCompleted: '10/27/21',
	})
}).then((response) => response.json()).then((json) => console.log(json));

// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch(urlOne,{
	method: 'DELETE'
});